#include "main.h"

void calculating()
{
	odd[0] = 1;
	odd[1] = odd[0] + 2;
	odd[2] = odd[1] + 2;
	odd[3] = odd[2] + 2;
	odd[4] = odd[3] + 2;
	odd[5] = odd[4] + 2;
	odd[6] = odd[5] + 2;
	odd[7] = odd[6] + 2;
	odd[8] = odd[7] + 2;
	odd[9] = odd[8] + 2;
}

void printing()
{
	printf("-----------------------------------------------------------------------------------------\n");
	printf("The first 10 odd numbers:\n%d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
		odd[0], odd[1], odd[2], odd[3], odd[4], odd[5], odd[6], odd[7], odd[8], odd[9]);
	printf("-----------------------------------------------------------------------------------------\n");
}

void main()
{
	calculating();
	printing();

	getch();
}
#include "main.h"

void calculating()
{
	even[0] = 0;
	even[1] = even[0] + 2;
	even[2] = even[1] + 2;
	even[3] = even[2] + 2;
	even[4] = even[3] + 2;
	even[5] = even[4] + 2;
	even[6] = even[5] + 2;
	even[7] = even[6] + 2;
	even[8] = even[7] + 2;
	even[9] = even[8] + 2;
}

void printing()
{
	printf("-----------------------------------------------------------------------------------------\n");
	printf("The first 10 even numbers:\n%d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
		even[0], even[1], even[2], even[3], even[4], even[5], even[6], even[7], even[8], even[9]);
	printf("-----------------------------------------------------------------------------------------\n");
}

void main()
{
	calculating();
	printing();

	getch();
}
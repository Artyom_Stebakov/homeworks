#include "main.h"

void calculate()
{
	fib[0] = 0;
	fib[1] = 1;
	fib[2] = 1;
	fib[3] = fib[1] + fib[2];
	fib[4] = fib[2] + fib[3];
	fib[5] = fib[3] + fib[4];
	fib[6] = fib[4] + fib[5];
	fib[7] = fib[5] + fib[6];
	fib[8] = fib[6] + fib[7];
	fib[9] = fib[7] + fib[8];
	fib[10] = fib[8] + fib[9];
	fib[11] = fib[9] + fib[10];
	fib[12] = fib[10] + fib[11];
	fib[13] = fib[11] + fib[12];
	fib[14] = fib[12] + fib[13];
	fib[15] = fib[13] + fib[14];
	fib[16] = fib[14] + fib[15];
	fib[17] = fib[15] + fib[16];
	fib[18] = fib[16] + fib[17];
	fib[19] = fib[17] + fib[18];
	fib[20] = fib[18] + fib[19];
}

void printing()
{
	printf("-----------------------------------------------------------------------------------------\n");
	printf("The first 20 Fibonacci numbers:\n%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d.\n",
		fib[1], fib[2], fib[3], fib[4], fib[5], fib[6], fib[7], fib[8], fib[9], fib[10], fib[11], fib[12], fib[13], fib[14], fib[15],
		fib[16], fib[17], fib[18], fib[19], fib[20]);
	printf("-----------------------------------------------------------------------------------------\n");
}

void main()
{
	calculate();
	printing();

	getch();
}
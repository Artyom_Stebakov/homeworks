#include "main.h"

void calculating()
{
	number[0] = 101;
	number[1] = number[0] - 1;
	number[2] = number[1] - 1;
	number[3] = number[2] - 1;
	number[4] = number[3] - 1;
	number[5] = number[4] - 1;
	number[6] = number[5] - 1;
	number[7] = number[6] - 1;
	number[8] = number[7] - 1;
	number[9] = number[8] - 1;
	number[10] = number[9] - 1;
}

void printing()
{
	printf("-----------------------------------------------------------------------------------------\n");
	printf("Last 10 numbers from 100:\n%d, %d, %d, %d, %d, %d, %d, %d, %d, %d\n",
		number[1], number[2], number[3], number[4], number[5], number[6], number[7], number[8], number[9], number[10]);
	printf("-----------------------------------------------------------------------------------------\n");
}

void main()
{
	calculating();
	printing();

	getch();
}